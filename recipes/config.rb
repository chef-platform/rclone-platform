#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

config = node[cookbook_name]['config']

config_file = node[cookbook_name]['config_file']
config_file_path = "#{node[cookbook_name]['config_dir']}/#{config_file}"

directory node[cookbook_name]['config_dir'] do
  user node[cookbook_name]['user']
  group node[cookbook_name]['user']
  mode '0755'
  recursive true
end

ini_conf = systemd_unit config_file do
  content config
  action :nothing
end.to_ini

file config_file_path do
  mode '0700'
  content ini_conf
end
