#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

package_retries = node[cookbook_name]['package_retries']

# executable is uncompressed in a dir named as the archive
executable = "#{node[cookbook_name]['prefix_home']}/rclone/"\
             "#{node[cookbook_name]['archive']}/rclone"

# unzip & rsync may not be installed by default
%w[unzip rsync].each do |mypackage|
  package mypackage do
    retries package_retries unless package_retries.nil?
  end
end

# Create prefix directories
[
  node[cookbook_name]['prefix_root'],
  node[cookbook_name]['prefix_home'],
  node[cookbook_name]['prefix_bin']
].uniq.each do |dir_path|
  directory "#{cookbook_name}:#{dir_path}" do
    path dir_path
    mode '0755'
    recursive true
    action :create
  end
end

# Install using ark
ark 'rclone' do
  action :install
  url node[cookbook_name]['mirror']
  prefix_root node[cookbook_name]['prefix_root']
  prefix_bin node[cookbook_name]['prefix_bin']
  prefix_home node[cookbook_name]['prefix_home']
  has_binaries []
  strip_components 0
  checksum node[cookbook_name]['checksum']
  version node[cookbook_name]['version']
  owner node[cookbook_name]['user']
  group node[cookbook_name]['group']
end

link "#{node[cookbook_name]['prefix_home']}/rclone/rclone" do
  to executable
  link_type :symbolic
  user node[cookbook_name]['user']
  group node[cookbook_name]['group']
end
