#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

cookbook_name = 'rclone-platform'

# Versions of rclone, needed for ark install
default[cookbook_name]['version'] = '1.39'
version = node[cookbook_name]['version']

default[cookbook_name]['architecture'] = 'linux-amd64'
architecture = node[cookbook_name]['architecture']

default[cookbook_name]['archive'] =
  "rclone-v#{version}-#{architecture}"

default[cookbook_name]['mirror'] =
  "https://downloads.rclone.org/v#{version}/"\
  "#{node[cookbook_name]['archive']}.zip"

default[cookbook_name]['checksum'] =
  '4bb194b67659312d9e33f043e3eb15144ba01877406ee8092438c19b18c2bbcb'

# User and group of rclone files / directories
default[cookbook_name]['user'] = 'root'
default[cookbook_name]['group'] = 'root'

# Where to put installation dir
default[cookbook_name]['prefix_root'] = '/opt'
# Where to link installation dir
default[cookbook_name]['prefix_home'] = '/opt'
# Where to link binaries
default[cookbook_name]['prefix_bin'] = '/opt/bin'

# Configuration directory
default[cookbook_name]['config_dir'] =
  "#{node[cookbook_name]['prefix_home']}/rclone/etc"

# Rclone configuration file
default[cookbook_name]['config_file'] = 'rclone.conf'

default[cookbook_name]['config'] = {
  # hash will be transformed to ini format describing a remote
  # 'ovh' => {
  #   'type' => 'swift',
  #   'user' => 'testtesttest',
  #   'key' => 'vdnvkvnsdlvnskNKNLKCn9',
  #   'auth' => 'https://auth.cloud.ovh.net/v2.0',
  #   'tenant' => '000000000007'
  # }
}

# Configure retries for the package resources, default = global default (0)
# (mostly used for test purpose)
default[cookbook_name]['package_retries'] = nil
