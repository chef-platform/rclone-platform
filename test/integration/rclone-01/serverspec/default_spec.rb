#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

ls_sftp = '/opt/rclone/rclone --config /opt/rclone/etc/rclone.conf ls test:'

describe command(ls_sftp) do
  its(:stdout) { should contain('0 1.test') }
  its(:stdout) { should contain('0 2.test') }
  its(:stdout) { should contain('0 3.test') }
  its(:stdout) { should contain('0 4.test') }
  its(:stdout) { should contain('0 5.test') }
  its(:stdout) { should contain('0 6.test') }
  its(:stdout) { should contain('0 7.test') }
  its(:stdout) { should contain('0 8.test') }
  its(:stdout) { should contain('0 9.test') }
  its(:stdout) { should contain('0 10.test') }
  its(:exit_status) { should eq 0 }
end
