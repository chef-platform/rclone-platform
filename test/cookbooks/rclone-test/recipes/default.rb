#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Install a ssh server
package 'openssh-server'

# And start sshd service
service 'sshd' do
  action :start
end

# create a test user
user 'test' do
  comment 'A user for tests'
  home '/home/test'
  shell '/bin/bash'
  manage_home true
  password '$1$peJ8V6nx$k3svlwPVquk1VJCwcOV.z0'
end

# create ten empty files in its home dir
(1..10).each do |test_file|
  file "/home/test/#{test_file}.test" do
  end
end
