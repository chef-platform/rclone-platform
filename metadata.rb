name 'rclone-platform'
maintainer 'Make.org'
maintainer_email 'sre@make.org'
license 'Apache-2.0'
description 'Cookbook to install and configure rclone'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/rclone-platform'
issues_url 'https://gitlab.com/chef-platform/rclone-platform/issues'
version '1.0.0'

supports 'centos', '>= 7.1'

chef_version '>= 12.19'

depends 'ark'
